
    <?php
      require ('animal.php');
      require ('frog.php');
      require ('ape.php');

      //release 0
      $sheep = new Animal("shaun");
      echo "Nama : $sheep->name <br>"; // "shaun"
      echo "Kaki : $sheep->legs <br>"; // 2
      echo "Cold blooded : $sheep->cold_blooded <br>"; // false

      //release 1
      // index.php
      $kodok = new Frog("buduk");
      $kodok-> legs = 4;
      echo "<br> Nama : $kodok->name <br>";
      echo "Kaki : $kodok->legs <br>";
      echo "Cold blooded : $kodok->cold_blooded <br>";
      $kodok->jump() ; // "hop hop"

      $sungokong = new Ape("kera sakti");
      echo "<br><br> Nama : $sungokong->name <br>";
      echo "Kaki : $sungokong->legs <br>";
      echo "Cold blooded : $sungokong->cold_blooded <br>";
      $sungokong->yell(); // "Auooo"

     ?>
